package week3day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class Login {

	//Login and Fill a form , submit it
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("http://leaftaps.com/opentaps/");
		//enter credential
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//Hit Submit button
		driver.findElementByClassName("decorativeSubmit").click();
		//Click a link in the page
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("KFC");
		driver.findElementById("createLeadForm_firstName").sendKeys("John");
		driver.findElementById("createLeadForm_lastName").sendKeys("Darby");
		driver.findElementByClassName("smallSubmit").click();
		driver.close();
		
		

	}

}
