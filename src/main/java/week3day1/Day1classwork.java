package week3day1;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Day1classwork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("http://leaftaps.com/opentaps/");
		//Check how many links available in the login page
		System.out.println("TASK1: Check how many links available in the login page");
		List<WebElement> leaftapreq = driver.findElementsByTagName("a");
		System.out.println(leaftapreq.size());
		for (WebElement printlinks : leaftapreq) {
			System.out.println(printlinks.getText());
		}

		//Click on the second link
		System.out.println("TASK2: Click on the second link in the Login page");
		leaftapreq.get(2).click();
		driver.close();

		System.out.println("TASK3: enter credential & Select a dropdown by visible text");
		//enter credential
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//Hit Submit button
		driver.findElementByClassName("decorativeSubmit").click();
		//Click a link in the page
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();

		WebElement ddbyID1 = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(ddbyID1);
		dd.selectByVisibleText("Conference");
		
		System.out.println("TASK4: enter credential & Select a dropdown by value");
		WebElement ddbyID2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(ddbyID2);
		dd1.selectByValue("CATRQ_AUTOMOBILE");

		System.out.println("TASK5: print all options from dropdown 2");
		List<WebElement> alloptns = dd1.getOptions();
		System.out.println(alloptns.size());
		for (WebElement eachoptn : alloptns) {
			System.out.println(eachoptn.getText());

		}

	}

}
