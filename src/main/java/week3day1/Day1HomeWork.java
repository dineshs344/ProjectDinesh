package week3day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Day1HomeWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("http://leaftaps.com/opentaps/");
		//Maximize the page
		driver.manage().window().maximize();
		//enter credential
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//Hit Submit button
		driver.findElementByClassName("decorativeSubmit").click();
		//Click CRM link in the page
		driver.findElementByLinkText("CRM/SFA").click();
		//Click Create Lead link in the page
		driver.findElementByLinkText("Create Lead").click();
		//Fill the form
		driver.findElementById("createLeadForm_companyName").sendKeys("KFC");
		driver.findElementById("createLeadForm_firstName").sendKeys("John");
		driver.findElementById("createLeadForm_lastName").sendKeys("Darby");
		WebElement ddbyID1 = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(ddbyID1);
		dd.selectByVisibleText("Conference");
		WebElement ddbyID2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(ddbyID2);
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("JohnLocal");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("DarbyLocal");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr.");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr.");
		driver.findElementById("createLeadForm_departmentName").sendKeys("QA");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("4.5M$");
		WebElement ddbyID3 = driver.findElementById("createLeadForm_currencyUomId");
		Select dd2 = new Select(ddbyID3);
		dd2.selectByValue("USD");
		WebElement ddbyID4 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd3 = new Select(ddbyID4);
		dd3.selectByVisibleText("Health Care");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("45");
		WebElement ddbyID5 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd4 = new Select(ddbyID5);
		dd4.selectByVisibleText("Public Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("SICCODE");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("XYZ");
		driver.findElementById("createLeadForm_description").sendKeys("Enter Description");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Enter Note");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("2");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9962985872");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("None");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("johndarby@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Matt Darby");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("XYZ");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("No 354");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("XYZ Street");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		WebElement ddbyID6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dd5 = new Select(ddbyID6);
		dd5.selectByVisibleText("India");
		//WAit for 30 seconds
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement ddbyID7 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dd6 = new Select(ddbyID7);
		dd6.selectByVisibleText("TAMILNADU");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600014");
		//Hit Create Buttton
		driver.findElementByClassName("smallSubmit").click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//Close the browser
		driver.close();
		

	}

}
