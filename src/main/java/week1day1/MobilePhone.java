package week1day1;

public class MobilePhone 
{
	static int mobileNumber ;
	public static String make = "MADE IN INDIA";
	static float price = 14500.00f;
	static int cameraPixel ;
	static Boolean isDualSIMEnabled ;
	static char is4GEnabled ;

	public static void addContact(int mblnumbr, String contactName)
	{
		//System.out.println("Contact "+contactName+" Added Successfully");
	}
	public static void screenlock()
	{
		System.out.println("Mobile Screen is Locked Successfully");
	}
	public static void makeacall(int contactno)
	{
		System.out.println("Call made to " +contactno+ " is Successfull");
	}
	public static void sendSMS(int contactno, String msg)
	{
		System.out.println("The Message '" +msg+"' has been sent to "+contactno+" Successfully");

	}
	static void deleteContact(String contactName)
	{
		System.out.println("Contact " +contactName+ " Deleted Successfully");
	}
	private static char chkconnectivity()
	{
		is4GEnabled = 'Y';
		return is4GEnabled;
	}
	static boolean chkabtSIM() 
	{
		isDualSIMEnabled = true;
		return isDualSIMEnabled;
	}
	static float chkprice()
	{
		return price;
	}
	static void updatecontact(String contactnme, String nickname)
	{
		System.out.println("Contact "+contactnme+"'s nickname has been updated as "+nickname);
	}
 	
	public static void main(String[] args) 
	{
		addContact(987654321, "xyz");	
		screenlock();
		makeacall(987451);
		sendSMS(9685742, "Hi, How are You");
		deleteContact("PersonXYZ");
		char answer1 = chkconnectivity();
		System.out.println("Phone is 4G Enabled Y/N: " +answer1);
		boolean answer2 = chkabtSIM();
		System.out.println("isDualSIMEnabled: " +answer2);
		float answer3 = chkprice();
		System.out.println("Price of the Phone is: " +answer3);
		updatecontact("Jack", "Friend");
		
	}
	
}
