package week1day2;

import java.util.Scanner;

public class PrintODDnumber 
{
	public static void main(String[] args) 
	{
		//Check the number entered is odd or even
		Scanner odd = new Scanner(System.in);
		System.out.println("Enter a number to check is ODD or EVEN:");
		int n = odd.nextInt();
		if(n%2==0)
		{
			System.out.println("The Number "+n+ " is a Even Number");
		}else
		System.out.println("The Number "+n+ " is a Odd Number");
		
		//Print the odd number of an array
		int[] numbr = {4,8,9,19};
		for (int i=0; i<numbr.length;i++)
		{
			if(numbr[i]%2!=0)
			{
				System.out.println("The Number "+numbr[i]+ " is a Odd Number");
			}
		}
	}
}
