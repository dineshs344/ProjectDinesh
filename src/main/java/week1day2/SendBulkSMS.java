package week1day2;

import week1day1.MobilePhone;

public class SendBulkSMS 
{
	public static void main(String[] args) 
	{
		MobilePhone mobObj2 = new MobilePhone();
		
		//Send same SMS to Different numbers using For Loop
		int Phonenumber[] = {900812441,900812442,900812443,900812444,900812445,900812446};
		for(int i=0; i < Phonenumber.length ;i++)
		{
			mobObj2.sendSMS(Phonenumber[i], "Hi");
		};
		
		//Send same SMS to Different numbers using For Each Loop
		int Phonenumbers[] = {900812441,900812442,900812443,900812444,900812445,900812446};
		for(int j:Phonenumbers)
		{
			mobObj2.sendSMS(j, "Hi");
		};

		//Send same message to different numbers by creating a new class and accessing the methods from other class
		mobObj2.sendSMS(900812441,"Good Morning!Happy Sunday");
		mobObj2.sendSMS(900812442,"Good Morning!Happy Sunday");
		mobObj2.sendSMS(900812443,"Good Morning!Happy Sunday");
		mobObj2.sendSMS(900812444,"Good Morning!Happy Sunday");
		mobObj2.sendSMS(900812445,"Good Morning!Happy Sunday");
		mobObj2.sendSMS(900812446,"Good Morning!Happy Sunday");
				
	}
}
