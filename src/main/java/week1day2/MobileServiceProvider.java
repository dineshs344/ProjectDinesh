package week1day2;
import java.util.Scanner;

public class MobileServiceProvider 
{
	public static void main(String[] args) 
	{
			//Method to check the provider using Using SCANNER Class & Input Runtime
			Scanner test = new Scanner(System.in);
			System.out.println("Enter phone number");
			int n5 = test.nextInt();
			if(n5==944)
			{
				System.out.println("BSNL");
			}
			else if(n5==900) 
			{
				System.out.println("Airtel");	
			}
			else if(n5==897) 
			{
				System.out.println("Idea");	
			}
			else if(n5==630) 
			{
				System.out.println("Jio");	
			}
			else 
			{
				System.out.println("No Provider");	
			};

			//Method to check the provider using Using Switch Case
			int Number = 944;
			String days ;
			switch(Number)
			{
			case 944: days = "BSNL";
			break;
			case 900: days = "AIRTEL";
			break;
			case 897: days = "Idea";
			break;
			case 630: days = "Jio";
			break;
			default : days = "No Provider";
			break;
			}
			System.out.println("Number starts with "+Number+ " is from " + days);

	}
}
