package week3day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Day2HomeWork1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("http://leaftaps.com/opentaps/");
		//Maximize the page
		driver.manage().window().maximize();
		//enter credential
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//Hit Submit button
		driver.findElementByClassName("decorativeSubmit").click();
		//Click a link in the page
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text() = 'Find Leads']").click();
		driver.findElementByXPath("(//input[@name = 'firstName'])[3]").sendKeys("John");
		driver.findElementByXPath("(//input[@name = 'lastName'])[3]").sendKeys("Darby");
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();

		//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//		WebDriverWait wait = new WebDriverWait(driver, 10);
		//		wait.until(ExpectedConditions.elementToBeClickable(By.tagName("td")));
		//		
		driver.findElementByXPath("(//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first '])[1]//a").click();


		//		
		//		driver.findElementByLinkText("Create Lead").click();
		//		driver.findElementById("createLeadForm_companyName").sendKeys("KFC");
		//		driver.findElementById("createLeadForm_firstName").sendKeys("John");
		//		driver.findElementById("createLeadForm_lastName").sendKeys("Darby");
		//		driver.findElementByClassName("smallSubmit").click();
		//		

	}

}
