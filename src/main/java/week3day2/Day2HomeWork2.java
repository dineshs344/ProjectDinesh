package week3day2;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day2HomeWork2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Maximize the page
		driver.manage().window().maximize();
		//Implicitly Wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//Hit the url
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC", Keys.TAB);
		boolean IsDateselected = driver.findElementById("chkSelectDateOnly").isSelected();
		if(IsDateselected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		//Find the table name
		WebElement table = driver.findElementByXPath("//table[@class = 'DataTable TrainList']");
		//Get the rows
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println("Total rows: "+rows.size());
		//Pick a row
		WebElement pickaRow = rows.get(1);
		//find the data/elements in the row picked
		List<WebElement> findElements = pickaRow.findElements(By.tagName("td"));
		System.out.println(findElements.get(1).getText());
		//print the first column from all the rows
		for (WebElement eachTR : rows) {
			String text = eachTR.findElements(By.tagName("td")).get(1).getText();
			System.out.println(text);

		}
	}

}
