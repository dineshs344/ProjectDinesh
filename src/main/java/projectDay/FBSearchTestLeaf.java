package projectDay;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import week4day2.SeMethods;

public class FBSearchTestLeaf extends Facebook_Method{
	
	String entertext = "TestLeaf";
	String button = "Like";
	
	@Test
	public void searchTL() throws InterruptedException {
		
		//Enter Search Text
		WebElement entersearch = locateElement("xpath", "//input[@class='_1frb\']");
		click(entersearch);
		type(entersearch, entertext);
		//Click Search
		WebElement tapsearch = locateElement("xpath", "//button[@class='_42ft _4jy0 _4w98 _4jy3 _517h _51sy _4w97']");
		click(tapsearch);
		Thread.sleep(5000);
		//Locate the Searched Element
		WebElement getdisplayed = locateElement("xpath", "(//div[@class='_52eh _5bcu'])[1]");
		String verifytext = getdisplayed.getText();
		//Verify the text of searched element
		if(verifytext.equals(entertext)) {
			System.out.println(verifytext);
		}
		//Locate Like button & get the text
		WebElement chklike = locateElement("xpath", "(//button[@type='submit'])[2]");
		String liketext = chklike.getText();
		//Hit Like if it is not liked & print sysout
		if(liketext.equals(button)) {
			click(chklike);	
			System.out.println("This Page is liked");
		}else
			System.out.println("This Page is Already liked");
		//Click the searched element
		click(getdisplayed);
		Thread.sleep(5000);
		//Page Title verification
		boolean verfyTitl = verifyTitle("TestLeaf - Home");
		if(verfyTitl) {
			System.out.println("Title is Verified");
		}else
			System.out.println("Title is not Verified");
		//Locate the element to get Like count & print the same
		WebElement chklikes = locateElement("xpath", "(//div[@class='clearfix _ikh'])[2]/div[2]");
		String likescount = chklikes.getText();
		System.out.println(likescount);
		
		
		
	}

}
