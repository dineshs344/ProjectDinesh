package projectDay;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.model.ConvertAnchor;
//import org.apache.commons.math3.analysis.function.Max;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import week4day2.SeMethods;

public class ZoomCar_Method extends SeMethods{
	
	private void launchZoomCarWebsite() {
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement journeylink = locateElement("xpath", "//a[text()='Start your wonderful journey']");
		click(journeylink);
		WebElement pickapoint = locateElement("xpath", "//div[@class = 'component-popular-locations']/div[2]");
		click(pickapoint);
		WebElement clickNext = locateElement("xpath", "//button[@class = 'proceed']");
		click(clickNext);
		WebElement selectDate = locateElement("xpath", "(//div[@class = 'day'])[1]");
		click(selectDate);
		WebElement clickNextagain = locateElement("xpath", "//button[@class = 'proceed']");
		click(clickNextagain);
		WebElement confirmDate = locateElement("xpath", "(//div[@class = 'day'])[1]");
		click(confirmDate);
		WebElement clickdone = locateElement("xpath", "//button[@class = 'proceed']");
		click(clickdone);
		List<WebElement> cabfares = driver.findElementsByXPath("(//div[@class='price'])");
		List<Integer> cabprice = new ArrayList<>();
		for (WebElement price : cabfares) {
			String text = price.getText().replaceAll("\\D", "");
			System.out.println(text);
			int parseInt = Integer.parseInt(text);
			cabprice.add(parseInt);
			
		}
		System.out.println("Total count of the results displayed: "+cabprice.size());
		
		
		
		Integer highfarecab = Collections.max(cabprice);
		System.out.println("Max Price" +highfarecab);
		
//		WebElement text = locateElement("xpath", "((((//div[contains(text(),'"+highfarecab+"')])/preceding::div)/preceding-sibling::div)//div)//div[@class = 'car-name']");
//		System.out.println(text);
		
		WebElement booknow = locateElement("xpath", "((//div[contains(text(),'"+highfarecab+"')])/following-sibling::button)");
		click(booknow);
			
		
	}
	
	@BeforeClass 
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription ="FB_LOGIN";
		category = "Smoke";
		author= "SACHIN";
	}
	
	@Test
	public void highFareCab() {
		launchZoomCarWebsite();
	}
}



