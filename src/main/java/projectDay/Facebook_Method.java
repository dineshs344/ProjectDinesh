package projectDay;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import week4day2.SeMethods;


public class Facebook_Method extends SeMethods {

	@BeforeMethod
	public void login() {

		startApp("chrome", "https://www.facebook.com/");
		WebElement eleUserName = locateElement("id", "email");
		type(eleUserName, "dineshs344");
		WebElement elePassword = locateElement("id","pass");
		type(elePassword, "Jhilludinesh");
		WebElement eleLogin = locateElement("xpath"," //input[@type='submit']");
		click(eleLogin);
	}

	@BeforeClass
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription ="FB_LOGIN";
		category = "Smoke";
		author= "SACHIN";
	}
		
	
	@AfterMethod
	public void close() {

		closeAllBrowsers();

	}

}