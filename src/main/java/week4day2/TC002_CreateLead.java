package week4day2;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import javax.sound.midi.SysexMessage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week5day1.ProjectMethods;
import week6day2.GetExcelData;

public class TC002_CreateLead extends  ProjectMethods{

	@BeforeClass
//	(groups = "common")
	public void setData() {
		testCaseName = "TC002";
		testCaseDescription ="Create lead";
		category = "Smoke";
		author= "Dinesh";
		excelfilename = "CreateLead";
	}

	@Test(dataProvider = "fetchdata")
//	@Test(groups = "smoke", dataProvider = "createlead")
//	@Test(invocationCount = 2, timeOut = 30000, dataProvider = "createlead")
	//@BeforeMethod
	public void createlead(String cname, String fname, String lname /*, String email, String pwd*/) {
		//login();
		WebElement leadcreate = locateElement("LinkText", "Create Lead");
		click(leadcreate);
		WebElement compname = locateElement("id", "createLeadForm_companyName");
		type(compname, cname);
		WebElement frstname = locateElement("id", "createLeadForm_firstName");
		type(frstname, fname);
		String Expectdfrstname = frstname.getText();
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, lname);
		WebElement drpdwn = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(drpdwn, "Employee");
		WebElement submit = locateElement("class","smallSubmit");
		click(submit);
		WebElement frstnamecreatd = locateElement("id", "viewLead_firstName_sp");
		String Actualfrstname = frstnamecreatd.getText();
		//Verify the Expected & Actual
		if(Actualfrstname.contains(Expectdfrstname))
		{
			System.out.println("Verification of First Name is Success");
		}else
			System.out.println("Verification of First Name is Failed");


	}
//	@DataProvider(name = "createlead")
//	public Object[][] getData(){
//
//		Object[][] data = new Object[2][3];
//		data[0][0] = "KFC";
//		data[0][1] = "Dinesh";
//		data[0][2] = "S";
////		data[0][3] = "d@d.com";
////		data[0][4] = "123";
//
//		data[1][0] = "Subway";
//		data[1][1] = "Chella";
//		data[1][2] = "TL";
////		data[1][3] = "c@c.com";
////		data[1][4] = "234";
//
//		return data;
//	}

	

}