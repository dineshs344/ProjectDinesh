package week4day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week5day1.ProjectMethods;

public class TC005_MergeLead extends /*TC003_EditLead*/ ProjectMethods {
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName = "TC005";
		testCaseDescription ="Merge lead";
		category = "Sanity";
		author= "Dinesh";
	}


//	@AfterMethod
	@Test(dependsOnMethods = "week4day2.TC002_CreateLead.createlead", enabled = false)
	public void mergeLead() throws InterruptedException {
	
		//login();
		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
		WebElement leadmenu = locateElement("LinkText", "Leads");
		click(leadmenu);
		WebElement mergelnk = locateElement("LinkText", "Merge Leads");
		click(mergelnk);
		WebElement imgicon1 = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(imgicon1);
		switchToWindow(1);
		WebElement LeadID = locateElement("xpath", "//input[@name = 'id']");
		type(LeadID, "10022");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		clickWithNoSnap(pickalead);
		switchToWindow(0);
		WebElement imgicon2 = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(imgicon2);
		switchToWindow(1);
		WebElement LeadID2 = locateElement("xpath", "//input[@name = 'id']");
		type(LeadID2, "10009");
		WebElement filtrleadagain = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrleadagain);
		Thread.sleep(5000);
		WebElement picklead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		clickWithNoSnap(picklead);
		switchToWindow(0);
		WebElement mrgelink = locateElement("LinkText", "Merge");
		clickWithNoSnap(mrgelink);
		acceptAlert();
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findlead);
		WebElement MergedLeadID = locateElement("xpath", "//input[@name = 'id']");
		type(MergedLeadID, "10022");
		WebElement filtrMrgdlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrMrgdlead);
		//closeBrowser();
		
		
	}
	
}
