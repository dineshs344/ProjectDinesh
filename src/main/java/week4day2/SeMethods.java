package week4day2;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.sound.midi.SysexMessage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import week5day1.LearnReports;

public class SeMethods extends LearnReports implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	ChromeOptions op;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
//				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Browser "+browser+" Launched Successfully", "pass");
//			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			reportStep("The Browser "+browser+" not Launched", "fail");
//			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}

	
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "LinkText" : return driver.findElementByLinkText(locValue);
			case "Name" : return driver.findElementByName(locValue);
			case "TagName" : return driver.findElementByTagName(locValue);
			case "PartialLinkText" : return driver.findElementByPartialLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element is not found", "fail");
//			System.err.println("The Element is not found");
		} catch (Exception e) {
			reportStep("Unknown Exception ", "fail");
//			System.err.println("Unknown Exception ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The data "+data+" is Entered Successfully", "pass");
//			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			reportStep("The data "+data+" is Not Entered", "fail");
//			System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}

	
	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" Clicked Successfully", "pass");
//			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			reportStep("The Element "+ele+"is not Clicked", "fail");
//			System.err.println("The Element "+ele+"is not Clicked");
		}
	}
	
	
	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" Clicked Successfully", "pass");
//			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
			reportStep("The Element "+ele+"is not Clicked", "fail");
//		System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		ele.getText();
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportStep("The DropDown Is Selected with VisibleText "+value, "pass");
//			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			reportStep("The DropDown Is not Selected with VisibleText "+value, "fail");
//			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);;
			reportStep("The DropDown Is Selected using index "+index, "pass");
//			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			reportStep("The DropDown Is not Selected using index  "+index, "fail");
//			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		String title = driver.getTitle();
		if(title.contains(expectedTitle)) {
		return true;
		}else
			return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		//driver.getWindowHandle();
		Set<String> allwindows1=driver.getWindowHandles();
//		System.out.println(allwindows1.size());
		List<String> listofWindows=new ArrayList<String>();//add all windows in a list
		listofWindows.addAll(allwindows1);//take all content
		String secondwindow=listofWindows.get(index); //2nd window  index 1
		driver.switchTo().window(secondwindow);
	}
	
	//Additional
	public void switchsingleWindow() {
		// TODO Auto-generated method stub
		String snglwindow=driver.getWindowHandle();
		driver.switchTo().window(snglwindow);
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
	}
	
	//Additional
	public void switchToFrame(int index) {
		driver.switchTo().frame(index);
	}
	
	//Additional
	public void switchToFrame(String locator) {
		driver.switchTo().frame(locator);
	}
	

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			reportStep("IOException", "fail");
//			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
	}

}
