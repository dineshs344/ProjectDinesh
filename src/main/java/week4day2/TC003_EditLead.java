package week4day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week5day1.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{
	
	@BeforeClass(groups = "common")
	public void setData() {
		testCaseName = "TC003";
		testCaseDescription ="Edit lead";
		category = "Sanity";
		author= "Dinesh";
		excelfilename = "EditLead";
	}

	@Test(dataProvider = "fetchdata")
//	@Test(groups="sanity", dependsOnGroups = "smoke")
//	@Test(dependsOnMethods = "week4day2.TC002_CreateLead.createlead")
	public void editlead(String fname, String cname) throws InterruptedException {

		//login();
		WebElement leadmenu = locateElement("LinkText", "Leads");
		click(leadmenu);
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findlead);
		WebElement leadname = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
		type(leadname, fname);
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		click(pickalead);
		String expectedTitle = driver.getTitle();
		verifyTitle(expectedTitle);
		WebElement editlink = locateElement("LinkText", "Edit");
		click(editlink);
		WebElement updtcompname = locateElement("id", "updateLeadForm_companyName");
		updtcompname.clear();
//		String text = "newcompanyname1";
		type(updtcompname, cname);
		WebElement updatelead = locateElement("xpath", "//input[@value = 'Update']");
		click(updatelead);
		WebElement updatedcmpnyname = locateElement("id", "viewLead_companyName_sp");
		String Actualcmpnyname = updatedcmpnyname.getText();
		//Verify the Expected & Actual
		if(Actualcmpnyname.contains(cname))
		{
			System.out.println("Verification of Company Name is Success");
		}else
			System.out.println("Verification of Company Name is Failed");
		//closeBrowser();
	}
}