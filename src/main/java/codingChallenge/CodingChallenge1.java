package codingChallenge;

//Week 1 - Day 1 Task

public class CodingChallenge1 
{
	//Multiplication table using 2 inputs with FOR Loop	
	public static void main(String[] args) {
		int a = 10;
		int b = 7;
		
		for(int i=1; i<=b ; i++)
		{
			System.out.println(i +" * " +a+" = " +i*a);
			
		};
		
	}

}
