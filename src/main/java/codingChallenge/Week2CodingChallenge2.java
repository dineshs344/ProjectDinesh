package codingChallenge;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Week2CodingChallenge2 {
	
	//Print the sum of array elements

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try
		{
			Scanner sum = new Scanner(System.in);
			System.out.println("Enter the number of elements in an array: ");
			int a = sum.nextInt();
			System.out.println("Enter the input elements: ");
			int[] b = new int[a];
			int s = 0;
			for(int i = 0; i<b.length; i++)
			{
				b[i] = sum.nextInt();
				s = s+b[i];

			}
			System.out.print("Sum of the input elements: "+ s);
		}
		catch(InputMismatchException e)
		{
			System.out.println("Invalid Input");}
	}

}
