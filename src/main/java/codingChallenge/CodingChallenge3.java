package codingChallenge;

import java.util.Scanner;

//Week 1 - Days 3 Task

public class CodingChallenge3 
{
	public static void main(String[] args) 
	{
		//Factorial using FOR LOOP
		Scanner test = new Scanner(System.in);
		System.out.println("Enter the Factorial Number : ");
		int Factorial = test.nextInt();
		int num = 1;
		
		for(int i=1; i<=Factorial; i++)
		{
			num = num*i;
		} System.out.println("Factorial of "+Factorial+ " is = "+ num);
	
		
		//Palindrome using WHILE & IF LOOP
		int a,b,c=0;
		Scanner test1 = new Scanner(System.in);
		System.out.println("Enter the Number to check if it is palindrome number or not:");
		int nmbr = test1.nextInt();
		b = nmbr;
		while(nmbr>0)
		{
			a = nmbr%10;
			c = (c*10) + a;
			nmbr = nmbr/10;
		}
		if(b==c)
		{
			System.out.println("The Number "+b+" is a Palindrome Number");
		}else
			System.out.println("The Number "+b+" is not a Palindrome Number");
	}

}
