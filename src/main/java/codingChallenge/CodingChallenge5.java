package codingChallenge;

import java.util.Scanner;

public class CodingChallenge5 
{
	public static void main(String[] args) 
	{
		//Print the Reverse number of the input
		int a,b,c=0;
		Scanner test1 = new Scanner(System.in);
		System.out.println("Enter the Number to print its reverse number:");
		int nmbr = test1.nextInt();
		b = nmbr;
		while(nmbr>0)
		{
			a = nmbr%10;
			c = (c*10) + a;
			nmbr = nmbr/10;
		}
		if(c>0)
		{
			System.out.println("The Reverse Number of "+b+" is :"+c);
		}

		//Print the Fibonacci Series
		int x=0,y=1,z,i,count=10;
		System.out.println("Fibonacci Series :");
		System.out.print(x+ " " +y);
		for(i=0; i<count; i++)
		{
			z = x+y;
			System.out.print(" "+z);
			x=y;
			y=z;
		}
		
	}

}
