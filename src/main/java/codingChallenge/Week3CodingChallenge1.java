package codingChallenge;

public class Week3CodingChallenge1 {

	//Print all the Armstrong number from 100 - 1000
	
	public static void arm(int a)
	{
		int digit1 = a/100;
		int digit2 = (a%100)/10;
		int digit3 = a%10;


		int arm1 = digit1*digit1*digit1;
		int arm2 = digit2*digit2*digit2;
		int arm3 = digit3*digit3*digit3;
		int armnum = arm1+arm2+arm3;

		if(armnum == a)
		{
			System.out.print(armnum+" ");
		}
	}
	public static void main(String[] args) {
		int a = 100, b = 1000;
		for(int i = a; i<b; i++)
		{
			arm(i);
		}
	}
}


