package codingChallenge;

import java.util.Arrays;
import java.util.Collections;

//Week 1 - Day 4 Task

public class CodingChallenge4 
{
	public static void main(String[] args) 
	{
		//Task 1 Using FOR LOOP & IF
	 	int i,a = 0;
		Integer[] array = {1,54,6,5,434,3,1,9};
		
		//ASC Order
		for(i=0; i<array.length; i++)
	 	{
	 		for (int j=i+1; j<array.length; j++) 
	 		{
	 			if(array[i] > array[j])
	 			{
	 				a=array[i];
	 				array[i]=array[j];
	 				array[j]=a;	 				 				
	 			}	 			
	 		}
	 	}
	 	System.out.println("Ascending order: " );
	 	for(i=0; i<array.length-1;i++)
	 	{
	 		System.out.print(array[i]+ ",");
	 	}
	 	System.out.println(array[array.length-1]);
	 	
	 	//DESC Order
	 	for(i=0; i<array.length; i++)
	 	{
	 		for (int j=i+1; j<array.length; j++) 
	 		{
	 			if(array[i] < array[j])
	 			{
	 				a=array[i];
	 				array[i]=array[j];
	 				array[j]=a;	 				 				
	 			}	 			
	 		}
	 	}
	 	System.out.println("Descending order: " );
	 	for(i=0; i<array.length-1;i++)
	 	{
	 		System.out.print(array[i]+ ",");
	 	}
	 	System.out.println(array[array.length-1]);
	 	
	 	
	 	//Task 1 Using Sort Keyword
	 	Integer[] arry = {1,54,6,5,434,3,1,9};
	 	//ASC Order
	 	Arrays.sort(arry);
		System.out.printf("Ascending Order :%s ",  Arrays.toString(arry));
		
		//DESC Order
		Arrays.sort(arry,Collections.reverseOrder());
		System.out.println("Descending Order = "+ Arrays.toString(arry));
		
		//Task 2 - Find the 1st & 3rd Largest Number in an array
		Integer[] number = {1,54,6,45};
		
		for(i=0; i<number.length; i++)
	 	{
	 		for (int j=i+1; j<number.length; j++) 
	 		{
	 			if(number[i] > number[j])
	 			{
	 				a=number[i];
	 				number[i]=number[j];
	 				number[j]=a;	 				 				
	 			}	 			
	 		}
	 	}
		System.out.println("First Largest Number of this Array is "+number[number.length-1]);
	 	System.out.println("Third Largest Number of this Array is "+number[number.length-3]);
	}
}
