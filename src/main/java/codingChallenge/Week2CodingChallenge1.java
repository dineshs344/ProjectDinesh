package codingChallenge;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Week2CodingChallenge1 {

	//Enter number to print the Prime number upto the input

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			Scanner prime = new Scanner(System.in);
			System.out.println("Enter the number for 'n': ");
			int n = prime.nextInt();
			List<Integer> b = new ArrayList<>();
			for(int i = 2; i<=n; i++)
			{		
				boolean flag = true;
				for(int a = 2; a<i ; a++)
				{
					if(i % a==0)
					{
						flag = false;
						break;
					}
				}
				if(flag)
				{
					b.add(i);
				}
				prime.close();
			}
			System.out.print(b);
		}
		catch(InputMismatchException e)
		{
			System.out.println("Invalid Input");
		}
	}
}

