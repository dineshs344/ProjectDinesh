package codingChallenge;

import java.util.Scanner;

//Week 1 - Day 2 Task

public class CodingChallenge2 
{
	
	public static void main(String[] args) {
		
		//Task 1
		
		int n =  1320;
		int m = 133;
		int j = 2900;
				
		//Check the largest of 3 numbers using LADDER IF

		if(n>m && n>j)
		{
			System.out.println(n);
		}
		else if(m>j)
		{
			System.out.println(m);
		}
		else
		{
			System.out.println(j);		
		}
		
		//Check the largest of 3 numbers using Ternary Operator
		int d = j > (n>m ? n:m) ? j : (n>m ? n:m);
		System.out.println(d);
		
		
		//Task 2
		//Using IF ELSE
				
		String month[] = {null, "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug"};
		int Days[] = {0,31,29,31,30,31,30,31,31};
		int a =7;
		
		if(a>0 && a<=8)
		{
			System.out.println("The " + a +"th Month of the Year is " +  month[a]);
			System.out.println("The Number of Days in the Month of " +month[a]+ " is " + Days[a]);
		}else
			System.out.println("Wrong Input");
		
		
		//Using Using Switch Case
		Scanner sample = new Scanner(System.in);
		System.out.println("Enter Number");
		int Number = 0;
		String Month = "Unknown";
		int days = sample.nextInt();
		
		switch(days)
		{
		case 1: 
			Month = "Jan"; 
			Number = 31;
			break;
		case 2: 
			Month = "Feb"; 
			Number = 29;
			break;
		case 3: 
			Month = "Mar"; 
			Number = 30;
			break;
		case 4: 
			Month = "Apr"; 
			Number = 31;
			break;
		case 5: 
			Month = "May"; 
			Number = 30;
			break;
		case 6: 
			Month = "Jun"; 
			Number = 31;
			break;
		case 7: 
			Month = "Jul"; 
			Number = 30;
			break;
		case 8: 
			Month = "Aug"; 
			Number = 31;
			break;
		case 9: 
			Month = "Sep"; 
			Number = 30;
			break;
		case 10: 
			Month = "Oct"; 
			Number = 31;
			break;
		case 11: 
			Month = "Nov"; 
			Number = 30;
			break;
		case 12: 
			Month = "Dec"; 
			Number = 31;
			break;
		}
		System.out.println("The " + days +"th Month of the Year is " +  Month);
		System.out.println("The Number of Days in the Month of " +Month+ " is " + Number);

	}
}
