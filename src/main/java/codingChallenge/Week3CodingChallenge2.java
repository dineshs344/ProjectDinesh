package codingChallenge;
public class Week3CodingChallenge2 {

	//Print the duplicate elements in an array
	
	public static void main(String[] args) {
		int[] a = {12,15,34,12,40,15,25};

		for (int i = 0; i<a.length; i++)
		{
			for (int j = i+1; j<a.length; j++)
			{
				if(a[i] == a[j])
				{
					System.out.print(a[i]+" ");
				}
			}
		}
	}
}

