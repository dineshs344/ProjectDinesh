package codingChallenge;

public class Week2CodingChallenge4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int a = 8, b = 16;
		for(int i = a; i<=b ; i++)
		{
			System.out.println(validateString(i));
		}

	}

	private static String validateString(int a)
	{
		if(a%3 == 0)
		{
			if(a%5 == 0)
			{
				return "FIZZBUZZ";
			}
			return "FIZZ";
		}else if(a%5 == 0)
		{
			return "BUZZ";
		}else
			return String.valueOf(a);
	}

}
