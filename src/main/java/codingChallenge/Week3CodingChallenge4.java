package codingChallenge;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Week3CodingChallenge4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("https://erail.in/");
		//Maximize the screen
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//from Station
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		//To Station
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		//for chk box
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if (selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		//find the table name
		WebElement table = driver.findElementByClassName("DataTable TrainList");
		List<WebElement> allrows = table.findElements(By.tagName("td"));
		System.out.println(allrows.get(1));
//		//Get the rows
//		List<WebElement> rows = findElementByLinkText.findElements(By.tagName("tr"));
//		System.out.println("Total rows: "+rows.size());
//		//Pick a row
//		WebElement pickaRow = rows.get(1);
//		//find the data/elements in the row picked
//		List<WebElement> findElements = pickaRow.findElements(By.tagName("td"));
//		System.out.println(findElements.get(1).getText());
//		//print the first column from all the rows
//		for (WebElement eachTR : rows) {
//			String text = eachTR.findElements(By.tagName("td")).get(1).getText();
//			System.out.println(text);
//	}

	}
}
