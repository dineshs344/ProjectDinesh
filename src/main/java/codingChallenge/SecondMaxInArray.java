package codingChallenge;

public class SecondMaxInArray {

	public static void main(String[] args) {
		int a[]={13,67,-88};
		int n=a.length;
		int big = Integer.MIN_VALUE;
		int secondbig = Integer.MIN_VALUE;
		for(int i=0;i<n;i++){
			if(big<a[i]){
				secondbig = big;
				big = a[i];
			} else if (a[i] != big && secondbig < a[i]){
				secondbig = a[i];
			}
		}
		System.out.println(big+" is the first largest number.");
		System.out.println(secondbig+" is the second largest number.");
	}

}
