package week2day1;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class ElectronicShop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<String> Electronics = new ArrayList<String>();
		
		//To Print the Count available in the shop use Size method
		System.out.println("Initial Count Available in the shop: "+Electronics.size());
		
		//To add items here use add method
		Electronics.add("Samsung");
		Electronics.add("LG");
		Electronics.add("Onida");
		Electronics.add("Sony");
		Electronics.add("Samsung");
		for (String itemsadded : Electronics) {
			System.out.println("Add a TV: "+itemsadded);
		}
		System.out.println("Count Available after adding items: "+Electronics.size());
		
		//To get the exact items from the shop use Get method
		System.out.println("The 3rd TV in the list added: " + Electronics.get(3));
		
		//To Remove a item from the shop use Remove method
		Electronics.remove("LG");
		for (String itemsadded : Electronics) {
			System.out.println("Print after removing a TV: "+itemsadded);
		}
		
		//Sort
		Collections.sort(Electronics);
		for (String itemsadded : Electronics) {
		System.out.println("Sorted: "+itemsadded);
	}
		//To delete all the items in the shop
		Electronics.clear();
		System.out.println("Count Available in the shop after deleting: "+Electronics.size());
	}

}
