package week2day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Day1HomeWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Given a string print unique chars
		Set<String> homework = new LinkedHashSet<String>();
		homework.add("C");
		homework.add("O");
		homework.add("G");
		homework.add("N");
		homework.add("I");
		homework.add("Z");
		homework.add("A");
		homework.add("N");
		homework.add("T");
		homework.add("I");
		homework.add("N");
		homework.add("D");
		homework.add("I");
		homework.add("A");

		System.out.println("Print the Unique Chars: "+ homework);

		//Given a string print chars in alpha order including duplicate
		List<String> homework1 = new ArrayList<String>();
		System.out.println("Size Before Adding: "+ homework1.size());

		homework1.add("A");
		homework1.add("M");
		homework1.add("A");
		homework1.add("Z");
		homework1.add("O");
		homework1.add("N");
		homework1.add("D");
		homework1.add("E");
		homework1.add("V");
		homework1.add("E");
		homework1.add("L");
		homework1.add("O");
		homework1.add("P");
		homework1.add("M");
		homework1.add("E");
		homework1.add("N");
		homework1.add("T");
		homework1.add("C");
		homework1.add("E");
		homework1.add("N");
		homework1.add("T");
		homework1.add("E");
		homework1.add("R");
		System.out.println("Size After Adding: "+ homework1.size());

		//Sort
		Collections.sort(homework1);
		for (String string : homework1) {
			System.out.println(string);

		}

		System.out.println("Size After Sorting: "+ homework1.size());

		//Given a string print the last char  of that string after sorting it in alpha order
		List<String> homework2 = new ArrayList<String>();
		System.out.println("Size Before Adding: "+ homework2.size());
		homework2.add("I");
		homework2.add("L");
		homework2.add("E");
		homework2.add("A");
		homework2.add("R");
		homework2.add("N");
		homework2.add("T");
		homework2.add("A");
		homework2.add("L");
		homework2.add("O");
		homework2.add("T");
		homework2.add("T");
		homework2.add("O");
		homework2.add("D");
		homework2.add("A");
		homework2.add("Y");
		System.out.println("Size After Adding: "+ homework2.size());

		//Get the last char after sorting
		Collections.sort(homework2);
		for (String string1 : homework2) {
			System.out.println(string1);

		}

		System.out.println("Size After Sorting: "+ homework2.size());
		int size = homework2.size() - 1;
		System.out.println("Pick the Last char after sorting: "+ homework2.get(size));

		//
		List<String> homework3 = new ArrayList<String>();
		String test = "I Learnt a Lot Today";
		System.out.println("Size Before Adding: "+ homework3.size());
		homework3.add(test);
		System.out.println("Size After Adding: "+ homework3.size());
		//Get the last char after sorting
		Collections.sort(homework3);
		for (String string1 : homework3) {
			System.out.println(string1);

		}
		System.out.println("Size After Sorting: "+ homework3.size());
		//Get the last char after sorting
		Collections.sort(homework3);
		for (String string1 : homework3) {
			System.out.println(string1);

		}
		int sizenew = homework3.size() - 1;
		System.out.println(test.charAt(sizenew));

	}

}
