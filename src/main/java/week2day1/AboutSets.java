package week2day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class AboutSets {

	public static void main(String[] args) {
		
		//Using TreeSet<> - Print the items in Alphabetical Order
		//Using HashSet<> - Print the items in Random Order
		//Using LinkedHashSet<> - Print the items in the Order added		
		
		Set<String> abtsets = new LinkedHashSet<String>();
		//To Print the Count available in the shop use Size method
		System.out.println("Initial Count Available in the shop: "+abtsets.size());

		//To add items here use add method
		abtsets.add("Samsung");
		abtsets.add("LG");
		abtsets.add("Onida");
		abtsets.add("Sony");
		abtsets.add("Samsung");
		for (String itemsadded : abtsets) {
			System.out.println("Add a TV: "+itemsadded);
		}
		System.out.println("Count Available after adding items: "+abtsets.size());
//
//		//To get the exact items from the shop use Get method
//		System.out.println("The 3rd TV in the list added: " + Electronics.get(3));
//
		//To Remove a item from the shop use Remove method
		abtsets.remove("LG");
		for (String itemsadded : abtsets) {
			System.out.println("Print after removing a TV: "+itemsadded);
		}

		//Again add a tv
		abtsets.add("Panasonic");
		
		//SET doesnt allow user to get a particular item - so use List here
		List<String> getItem = new ArrayList<String>();
		getItem.addAll(abtsets);
		System.out.println(getItem.get(1));
	}

}

