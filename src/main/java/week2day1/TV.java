package week2day1;

public interface TV {

	public void poweron();
	public void changechannel(int channelnumber);
	public void changechannel(String channelname);
	public void increasevolume(int volumebutton);
	public void poweroff();
}
