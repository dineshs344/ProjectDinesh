package week2day1;

public class Samsung implements TV{

	@Override
	public void poweron() {
		// TODO Auto-generated method stub
		System.out.println("Television is switched on Successfully");
		
	}

	@Override
	public void changechannel(int channelnumber) {
		// TODO Auto-generated method stub
		System.out.println("Television is changed to Channel No "+ channelnumber);
		
	}

	@Override
	public void changechannel(String channelname) {
		// TODO Auto-generated method stub
		System.out.println("Television is changed to "+ channelname +" Channel Successfully");
		
	}

	@Override
	public void poweroff() {
		// TODO Auto-generated method stub
		System.out.println("Television is switched off");
		
	}

	@Override
	public void increasevolume(int volumebutton) {
		// TODO Auto-generated method stub
		System.out.println("Volume of the Television is increased to " +volumebutton);
		
	}

}
