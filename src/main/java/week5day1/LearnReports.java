package week5day1;
import java.io.IOException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	//	public static void main(String[] args) throws IOException {
	//		
	//		// TODO Auto-generated method stub
	//		
	//		//create UN Editable HTML file
	//		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
	//		//history
	//		html.setAppendExisting(true); 
	//
	//		//editable
	//		ExtentReports extent = new ExtentReports();
	//		extent.attachReporter(html);
	//
	//		//inputs
	//
	//		ExtentTest test= extent.createTest("TC002_CreatLead","Creat") ;
	//		test.assignAuthor("Sachin");
	//		test.assignCategory("Smoke");
	//		test.pass("The data demosalesManager entered succsessfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	//		test.pass("The data crmsfa is entered succsss");
	//		test.fail("Login button failed");
	//
	//		//flush erase memory
	//		extent.flush();
	
	
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName, testCaseDescription, 
					author, category, excelfilename;
	
	@BeforeSuite(groups = "common") // One time execution 
	public void startResult() {
		ExtentHtmlReporter html = new ExtentHtmlReporter
				("./reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	@BeforeMethod(groups = "common")
	public void startTestCase() {
		test = extent
				.createTest(testCaseName, testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void reportStep(String desc,String status) {
		if (status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}if (status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		}
	}
	
	@AfterSuite(groups = "common")
	public void stopResult(){		
		extent.flush();
	}




}

