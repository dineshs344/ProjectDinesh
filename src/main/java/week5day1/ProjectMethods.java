package week5day1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import week4day2.SeMethods;
import week6day2.GetExcelData;

public class ProjectMethods extends SeMethods {
	
//	@BeforeClass
//	public void testq() {
//		System.out.println("b4test");
//	}
	
	
	@DataProvider(name = "fetchdata")
	public Object[][] getData() throws IOException{
		Object[][] exceldata = GetExcelData.getdatafromexcel(excelfilename);
		return exceldata;
	}
	
	@BeforeMethod
//	(groups = "common")
	@Parameters({"browser", "username", "appurl","password"})
	public void login(String browsername, String uname, String url, String pwd ) {
		startApp(browsername, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crmlink = locateElement("LinkText", "CRM/SFA");
		click(crmlink);
	}
	
	@AfterMethod(groups = "common")
	public void close() {
		closeAllBrowsers();		
	}

}
