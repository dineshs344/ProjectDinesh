package week6day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) throws IOException {

		//get workbook
		XSSFWorkbook wBook = new XSSFWorkbook("./data/CreateLead.xlsx");
		//get sheet
		XSSFSheet sheet = wBook.getSheet("createlead");
		//get last row number 
		int lastRowNum = sheet.getLastRowNum();
		//get last column number
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		//iterate for all the rows, it should start with i = 1, since 0 will be header
		for(int i = 1; i<=lastRowNum; i++) {
			//this will iterate all the row
			XSSFRow row = sheet.getRow(i);
			//iterate for all the columns, it should start with j = 0, since there will not be any header
			for(int j = 0; j<lastCellNum; j++) {
				//this will iterate all the column
				XSSFCell cell = row.getCell(j);
				//this will get the value from each cell
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
			}
		}
		wBook.close();
	}
}
