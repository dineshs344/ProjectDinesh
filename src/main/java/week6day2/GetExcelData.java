package week6day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GetExcelData {

	public static Object[][] getdatafromexcel (String fname) throws IOException {
		// TODO Auto-generated method stub
		//get workbook
		XSSFWorkbook wBook = new XSSFWorkbook("./data/"+fname+".xlsx");
		//get sheet
		XSSFSheet sheet = wBook.getSheetAt(0);
		//get last row number 
		int lastRowNum = sheet.getLastRowNum();
		//get last column number
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		
		//Create object and assign the array values of rows n columns
		Object[][] data = new Object[lastRowNum][lastCellNum];
		
		//iterate for all the rows, it should start with i = 1, since 0 will be header
		for(int i = 1; i<=lastRowNum; i++) {
			//this will iterate all the row
			XSSFRow row = sheet.getRow(i);
			//iterate for all the columns, it should start with j = 0, since there will not be any header
			for(int j = 0; j<lastCellNum; j++) {
				//this will iterate all the column
				XSSFCell cell = row.getCell(j);
				//this will get the value from each cell
				String stringCellValue = cell.getStringCellValue();
				//save the cell value in the row/column, i-1 is because j iterates from 0 and i from 1
				data[i-1][j] = stringCellValue;
				System.out.println(stringCellValue);
			}
		}
		wBook.close();
		return data;
	}

	}


